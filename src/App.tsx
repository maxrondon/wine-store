import React, { Component } from "react";
import './App.css';
import * as THREE from "three";
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader';
import { MTLLoader } from 'three/examples/jsm/loaders/MTLLoader';

const ROOMMTL = require('./assets/room.mtl');
const ROOMOBJ = require('./assets/room.obj');
const SHELFOBJ = require('./assets/shelf.obj');
const SHELFMTL = require('./assets/shelf.mtl');
const BOTTLEOBJ = require('./assets/botella.obj');
const BOTTLEMTL = require('./assets/botella.mtl');
// const FLOOR = require('./assets/floor.jpg');

// var MTLFile = 'https://poly.googleapis.com/downloads/fp/1563011020793397/65Hf2EMEo4s/4iFmTtaWWut/248_Bottle%20Of%20Wine.mtl';

const bottle = (scene: any, x: number, y: number, z: number, angle: number) => {
  var mtlLoader = new MTLLoader();
  mtlLoader.setTexturePath(process.env.PUBLIC_URL +'/assets/')
  mtlLoader.load( BOTTLEMTL, function( materials ) {
    materials.preload();
    var objLoader = new OBJLoader();
    objLoader.setMaterials( materials );
    objLoader.load( BOTTLEOBJ, async function ( object ) {
    object.scale.set(1.5 , 1.5, 1.5)
    object.rotation.set(0, -angle + Math.random()*0.1*(Math.random() > 0.5 ? 1 : -1)  ,  0)
    object.position.set(x, y, z);
    object.castShadow = true;

    object.traverse( function ( node : any ) {
      if (node instanceof THREE.Mesh) {
        node.receiveShadow = true;
        node.castShadow = true;
      }
    });

    object.receiveShadow = true;
       scene.add( object );
    });
    //, onProgress, onError );
  });
}

const loadShelf = (scene: any, x: number, y: number, z: number, angle: number) => {
  ////////////////////////
  var shelfMtlLoader = new MTLLoader();
  var shelfObjLoader = new OBJLoader();

  var map2 = new THREE.TextureLoader().load( "/assets/wood2.jpg" )
  // map2.rotation = Math.PI/2.84;
  // map2.wrapS = map.wrapT = THREE.RepeatWrapping;
  // map2.repeat.set(8, 8);

  shelfMtlLoader.load( SHELFMTL, async function( materials ) {
    await materials.preload();
    shelfObjLoader.setMaterials( materials );
    shelfObjLoader.load( SHELFOBJ, function ( object ) {

      object.rotation.set(0, angle , 0);
      object.position.set(x, y, z);
      object.traverse( (node: any ) => {
        if ( node instanceof THREE.Mesh   ) {
          node.castShadow = true;
          node.receiveShadow = true;
          ((node as any).material as any).map = map2;
        }
      });

      object.scale.set(3.5 , 3.5, 3.5)
      object.castShadow = true;

      scene.add( object );
    });


    for (var i = 0; i < 14; i++) {
      if (!angle) {
        // TOP
        bottle(scene, x + 0.4, 1.7 ,y + i*0.35 + 0.5, angle);
        bottle(scene, x + 0.05, 1.7 ,y + i*0.35 + 0.4, angle);
        // bottle(scene, x - 0.3, 1.7 ,y + i*0.35 + 0.6);

        // MIDTOP
        Math.random() > 0.7 && bottle(scene, x + 0.4, 0.3 ,y + i*0.35 + 0.5, angle);
        bottle(scene, x + 0.05, 0.3 ,y + i*0.35 + 0.4, angle);
        // bottle(scene, x - 0.3, 0.3 ,y + i*0.35 + 0.6);

        // MIDBOT
        Math.random() > 0.6 && bottle(scene, x + 0.4, -1.4 ,y + i*0.35 + 0.5, angle);
        bottle(scene, x + 0.05, -1.4 ,y + i*0.35 + 0.4, angle);
        // bottle(scene, x - 0.3, -1.4 ,y + i*0.35 + 0.6);

        // BOT
        bottle(scene, x + 0.4, -2.8 ,y + i*0.35 + 0.5, angle);
        bottle(scene, x + 0.05, -2.8 ,y + i*0.35 + 0.4, angle);
        // bottle(scene, x - 0.3, -1.4 ,y + i*0.35 + 0.6);

      } else {
        // TOP
        Math.random() > 0.3 && bottle(scene, x -2.3 + i*0.35, 1.7, y, angle);
        bottle(scene, x -2.4 + i*0.35, 1.7, y - 0.35, angle);
        bottle(scene, x -2.2 + i*0.35, 1.7, y - 0.7, angle);

        // MIDTOP
        Math.random() > 0.7 && bottle(scene, x -2.3 + i*0.35, 0.3, y, angle);
        bottle(scene, x -2.4 + i*0.35, 0.3, y - 0.35, angle);
        bottle(scene, x -2.2 + i*0.35, 0.3, y - 0.7, angle);

        // MIDBOT
        Math.random() > 0.6 && bottle(scene, x -2.3 + i*0.35, -1.4, y, angle);
        bottle(scene, x -2.4 + i*0.35, -1.4, y - 0.35, angle);
        bottle(scene, x -2.2 + i*0.35, -1.4, y - 0.7, angle);

        // BOT
        Math.random() > 0.3 && bottle(scene, x -2.3 + i*0.35, -2.8, y, angle);
        bottle(scene, x -2.4 + i*0.35, -2.8, y - 0.35, angle);
        bottle(scene, x -2.2 + i*0.35, -2.8, y - 0.7, angle);
      }
    }

  });
}

let direction = 1;

class App extends Component {
  public mount: any;
  componentDidMount() {
    var scene = new THREE.Scene();
    var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 100 );
    camera.position.z = 3.7;
    camera.position.x = 0;
     camera.position.y = 0;
    // camera.rotation.x = -90 * Math.PI / 180
    // camera.position.z = 3;
    // camera.position.y = 10;

    // camera.position.y = 15;
    var renderer = new THREE.WebGLRenderer();
    renderer.shadowMapEnabled = true;
    renderer.setSize( window.innerWidth, window.innerHeight );
    // document.body.appendChild( renderer.domElement );
    // use ref as a mount point of the Three.js scene instead of the document.body
    this.mount.appendChild( renderer.domElement );



    var spotLight = new THREE.SpotLight( 0xffffff );
    spotLight.position.set( 1, 3.5, 4 );
    spotLight.intensity = 0.5;
    spotLight.castShadow = true;
    scene.add(spotLight);
    scene.add(spotLight.target);
    // const helper = new THREE.SpotLightHelper(spotLight);
    // scene.add(helper);


    const color = 0xFFFFFF;
    const intensity = 1;
    const light = new THREE.PointLight(color, intensity);
    light.position.set(0, 10, 0);

    scene.add(light);

    var light2 = new THREE.AmbientLight( 0xffffff, 0.25 );
    // light2.position.set( 0, 1000, 1000 );
    scene.add( light2 );



    var mtlLoader = new MTLLoader();
    var roomLoader = new OBJLoader();
    // roomLoader.setMaterials( materials );
    var map = new THREE.TextureLoader().load( "/assets/wall2.jpg" )
    map.rotation = Math.PI/2.84;
    map.wrapS = map.wrapT = THREE.RepeatWrapping;
    map.repeat.set(8, 8);


    mtlLoader.setTexturePath(process.env.PUBLIC_URL +'/assets/')
    mtlLoader.load( ROOMMTL, async function( materials ) {
      materials.preload();
      roomLoader.setMaterials( materials );
      roomLoader.load( ROOMOBJ, function ( room ) {
        room.scale.set(5 , 5, 5)
        room.position.set(15, 0, 13);
        room.rotation.set(0, Math.PI , 0);

        room.traverse( function ( node : any ) {
          if ( node.isMesh && node.material.name === 'mat15' ) {
            node.material.map = map;
          } else if ( node.isMesh && node.material.name === 'mat16' ) {
            node.material.map.rotation = Math.PI/1.64;
          }
          if (node instanceof THREE.Mesh) {
            node.receiveShadow = true;
          }

        });
        room.receiveShadow = true;


        scene.add( room );
      });
    });

    loadShelf(scene, -0.7, -0.6, -1, Math.PI/2);
    loadShelf(scene, 4.8, -0.6, -1, Math.PI/2);
    loadShelf(scene, -4.15, -0.6, 2.3, 0);


    var animate = function () {
      requestAnimationFrame( animate );
      if (camera.rotation.y > 0.7) {
        direction = -1;
      }
      if (camera.rotation.y < -0.1) {
        direction = 1;
      }
      camera.rotation.y += 0.001 * direction;
      renderer.render( scene, camera );
    };
    animate();

  }
  render() {
    return (
      <div ref={ref => (this.mount = ref)} />
    )
  }
}

export default App;
